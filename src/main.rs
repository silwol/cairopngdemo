extern crate cairo;

use cairo::{
    Context,
    ImageSurface,
    Format
};
use std::fs::File;

fn main() {
    let surface = ImageSurface::create(Format::ARgb32, 100, 100);

    let f = File::create("test.png").unwrap();

    let context = Context::new(&surface);

    context.set_source_rgb(1.0, 0.0, 0.0);
    context.move_to(10.0, 10.0);
    context.line_to(90.0, 90.0);
    context.move_to(90.0, 10.0);
    context.line_to(10.0, 90.0);
    context.stroke();

    surface.write_to_png(f).unwrap();
}
